matplotlib==3.5.0
numpy==1.21.4
protobuf==3.19.2
pwlf==2.0.4
scikit_learn==1.0.2
torch==1.10.0
torchvision==0.11.1
