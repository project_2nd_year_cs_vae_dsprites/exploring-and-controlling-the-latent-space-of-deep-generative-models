## Exploring and controlling the latent space of deep generative models

## Description
Deep generative models is a hot topic in Machine Learning these days. Most of those models follow the same method to generate images (or sound, ...) which is to start from a space called latent space, pick an element (vector) and pass it through a decoder to get a generated image. The latent space is often assimilated to a "white noise" because it is hard to find an exploitable structure in it. But sometimes we want to modify something in the generated image, and to do that, we need to explore and control the latent space.

    For example if our model generate cats, we may want to change the color, the size or the lenth of the tail.
    Here is an oversimplyfied example with a simple  and well structured 2D latent space.
    
![cat](images/cat.JPG)
    
In our project we try to do it with a popular generation model : variationnal autoencoder (VAE) and with the dSprites dataset.

## Visuals

![VAE_reconstruction](images/output.png)

First square shifted down
![PCA_modify](images/output1.png)

## Installation

- Using Google Colab (recommanded) :
    1. Clone the repository
    2. In your Google Drives, create a folder in My Drive named "Projet_VAE_dSprites" and add the files "current_best_model.pt", current_checkpoint.pt" and the datafile "dsprites_ndarray_co1sh3sc6or40x32y32_64x64.npz" in the folder.
    3. Open the notebook in colab, check that in the section "Initializing the notebook" and in the subsection "Import Drive and select path" we have <code>colab = True</code>
    4. Run the notebook

- On a local computer :
    1. Clone the repository
    2. Install the python packages <code>pip install -r requirements.txt</code>
    3. Open the notebook in colab, check that in the section "Initializing the notebook" and in the subsection "Import Drive and select path" we have <code>colab = True</code> and give to the variable '''path''' your current path
    4. Run the notebook

## Roadmap
We failed generalizing the modification for all the images. So we are looking for some methods to fix that (to continue)



